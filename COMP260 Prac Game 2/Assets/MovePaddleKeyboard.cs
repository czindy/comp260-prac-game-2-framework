﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddleKeyboard : MonoBehaviour {

	private Rigidbody rigidbody;
	public string XAxisToUse;
	public string YAxisToUse;
	private Vector3 direction;
	public float speed = 20f;
	public float force = 10f;

	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;
	}

	void Update(){
		
	}


	void FixedUpdate() {
		//get the input values
	

		direction.x = Input.GetAxis(XAxisToUse);
		direction.z = Input.GetAxis(YAxisToUse);

		Vector3 pos = direction;
		Vector3 dir = direction;
		Vector3 vel = dir.normalized * speed;

		//check if this speed is going to overshoot the target
		float move = speed * Time.fixedDeltaTime;
		float distToTarget = dir.magnitude;

		if (move > distToTarget) {
			//scale the velocity down appropriately
			vel = vel * distToTarget / move;
		}

		rigidbody.velocity = vel;
		//rigidbody.AddForce(dir.normalized * force);
	}






}
